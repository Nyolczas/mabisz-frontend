function scroll() {
    var scroll = window.scrollY;
    var slow, frame;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

    //==================== section Start START ========================================
    //--- social navbar
    var headerSocial = document.getElementById('headerSocial');

    if (scroll > window.innerHeight) {
        headerSocial.classList.add('header-social-off');
    } else {
        headerSocial.classList.remove('header-social-off');
    }

    //--- sc Start wrapper offset 
    var scStartWrapper = document.getElementById('scStartWrapper');

    if (scroll > 0) {
        scStartWrapper.classList.remove('start-spacer');
    } else {
        scStartWrapper.classList.add('start-spacer');
    }
    //==================== section Start END ========================================

    //==================== section 20 START ========================================
    var sc20 = document.getElementById('section20');
    var sc20Start = section20.offsetTop - height;
    var sc20End = sc20Start + sc20.clientHeight;
    var sc20Height = sc20End - sc20Start;

    //--- left 1
    var sc20left1= document.getElementById('sc20LeftWrapper');
    if(scroll > (sc20Start + (sc20Height * 0.29)) && scroll < sc20End) {
        sc20left1.classList.remove('left-out');
    } else {
        sc20left1.classList.add('left-out');
    }

    //--- sc START anim eltüntetése
    if(scroll > (sc20Start + (sc20Height * 0.29))) {
        scStartWrapper.style.visibility = "hidden";
    } else {
        scStartWrapper.style.visibility = "visible";
    }

    //--- right wrapper
    var sc20Right = document.getElementById('sc20RightWrapper');
    if(scroll > (sc20Start + (sc20Height * 0.45)) && scroll < sc20End) {
        sc20Right.classList.remove('right-out');
    } else {
        sc20Right.classList.add('right-out');
    }
    
    //==================== section 20 END ========================================

    //==================== section 30 START ========================================
    var sc30 = document.getElementById('section30');
    var sc30Start = section30.offsetTop - height;
    var sc30End = sc30Start + sc30.clientHeight;
    var sc30Height = sc30End - sc30Start;

    //--- left 1
    var sc30left1= document.getElementById('sc30LeftWrapper');
    if(scroll > (sc30Start + (sc30Height * 0.25)) && scroll < sc30End) {
        sc30left1.classList.remove('left-out');
    } else {
        sc30left1.classList.add('left-out');
    }

    //--- right wrapper
    var sc30Right = document.getElementById('sc30RightWrapper');
    if(scroll > (sc30Start + (sc30Height * 0.45)) && scroll < sc30End) {
        sc30Right.classList.remove('right-out');
    } else {
        sc30Right.classList.add('right-out');
    }
    //==================== section 30 END ========================================

    //==================== section 40 START ========================================
    var sc40 = document.getElementById('section40');
    var sc40Start = section40.offsetTop - height;
    var sc40End = sc40Start + sc40.clientHeight;
    var sc40Height = sc40End - sc40Start;

    //--- left 1
    var sc40left1= document.getElementById('sc40LeftWrapper');
    if(scroll > (sc40Start + (sc40Height * 0.29)) && scroll < sc40End) {
        sc40left1.classList.remove('left-out');
    } else {
        sc40left1.classList.add('left-out');
    }

    //--- right wrapper
    var sc40Right = document.getElementById('sc40RightWrapper');
    if(scroll > (sc40Start + (sc40Height * 0.45)) && scroll < sc40End) {
        sc40Right.classList.remove('right-out');
    } else {
        sc40Right.classList.add('right-out');
    }
    //==================== section 40 END ========================================

    //==================== section 50 START ========================================
    var sc50 = document.getElementById('section50');
    var sc50Start = section50.offsetTop - height;
    var sc50End = sc50Start + sc50.clientHeight;
    var sc50Height = sc50End - sc50Start;

    //--- left 1
    var sc50left1= document.getElementById('sc50LeftWrapper');
    if(scroll > (sc50Start + (sc50Height * 0.29)) && scroll < sc50End) {
        sc50left1.classList.remove('left-out');
    } else {
        sc50left1.classList.add('left-out');
    }

    //--- right wrapper
    var sc50Right = document.getElementById('sc50RightWrapper');
    if(scroll > (sc50Start + (sc50Height * 0.45)) && scroll < sc50End) {
        sc50Right.classList.remove('right-out');
    } else {
        sc50Right.classList.add('right-out');
    }
    //==================== section 50 END ========================================

    //==================== section 60 START ========================================
    var sc60 = document.getElementById('section60');
    var sc60Start = section60.offsetTop - height;
    var sc60End = sc60Start + sc60.clientHeight;
    var sc60Height = sc60End - sc60Start;

    //--- left 1
    var sc60left1= document.getElementById('sc60LeftWrapper');
    if(scroll > (sc60Start + (sc60Height * 0.29)) && scroll < sc60End) {
        sc60left1.classList.remove('left-out');
    } else {
        sc60left1.classList.add('left-out');
    }

    //--- right wrapper 
    var sc60Right = document.getElementById('sc60RightWrapper');
    if(scroll > (sc60Start + (sc60Height * 0.45)) && scroll < sc60End) {
        sc60Right.classList.remove('right-out');
    } else {
        sc60Right.classList.add('right-out');
    }

    //==================== section 60 END ========================================

    //==================== section Logo START ========================================
    var tovabbi = document.getElementById('tovabbi');
    var tanacsok = document.getElementById('tanacsok');

    if(scroll > sc60End) {
        tovabbi.classList.remove('tovabbi-out');
        tanacsok.classList.remove('tanacsok-out');
    } else {
        tovabbi.classList.add('tovabbi-out');
        tanacsok.classList.add('tanacsok-out');
    }

    //==================== section Logo END ========================================

    //==================== character animations START ========================================
    //--- sc Start
    slow = 50; // min = 1
    frame = Math.round((scroll / slow) % 12);
    var animStart = document.getElementById('scStart');
    animStart.style.backgroundPositionX = (frame * animStart.offsetWidth * -1 + "px");

    //--- sc 20
    slow = 110; // min = 1
    frame = Math.round((scroll / slow) % 12);
    var anim20 = document.getElementById('sc20');
    anim20.style.backgroundPositionX = (frame * anim20.offsetWidth * -1 + "px");

    //--- sc 30 
    slow = 110; // min = 1
    frame = Math.round((scroll / slow) % 12);
    var anim30 = document.getElementById('sc30');
    anim30.style.backgroundPositionX = (frame * anim30.offsetWidth * -1 + "px");

    //--- sc 40
    slow = 90; // min = 1
    frame = Math.round((scroll / slow) % 12);
    var anim40 = document.getElementById('sc40');
    anim40.style.backgroundPositionX = (frame * anim40.offsetWidth * -1 + "px");

    //--- sc 50
    slow = 90; // min = 1
    frame = Math.round((scroll / slow) % 12);
    var anim50 = document.getElementById('sc50');
    anim50.style.backgroundPositionX = (frame * anim50.offsetWidth * -1 + "px");

    //--- sc 60
    slow = 90; // min = 1
    frame = Math.round((scroll / slow) % 12);
    var anim60papa = document.getElementById('sc60papa');
    anim60papa.style.backgroundPositionX = (frame * anim60papa.offsetWidth * -1 + "px");
    var anim60mama = document.getElementById('sc60mama');
    anim60mama.style.backgroundPositionX = (frame * anim60papa.offsetWidth * -1 + "px");

    //--- karakterek emelése
    var animWrapper60 = document.getElementById('animWrapper60');
    var liftTrigger = sc60Start + (sc60Height * 0.92);
    if(scroll >=  liftTrigger) {
        animWrapper60.style.top = liftTrigger - scroll + (height * 0.5) + 'px';
    }

    //==================== character animations END ========================================
}